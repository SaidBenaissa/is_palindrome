//
//  main.cpp
//  isPalindrome
//
//  Created by SAID on 10/01/2021.
//
#include <iostream>
#include <cmath>        // std::abs
using namespace std;

class Solution {
public:
    int rev(int x){
        int rev = 0;
        int pop;
        
        while(x != 0){
            pop = x % 10;
            x /=10;
            if (rev > INT_MAX / 10 || (rev == INT_MAX / 10 && pop > 7)) return 0;
            if (rev < INT_MIN / 10 || (rev == INT_MIN / 10 && pop < -8)) return 0;
            rev = (rev * 10) + pop;
        }
        
        return rev;
    }
    
    bool isPalindrome(int x) {
        
        if(abs(x)==rev(x))
            return true;
        else
            return false;
        
    }
};

